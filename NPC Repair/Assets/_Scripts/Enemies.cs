﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    public Transform spawnPoint;
    private float health = 200;

    // Start is called before the first frame update
    void Start()
    {
        // PLAY INTRO ANIMATION \\
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            // PLAY DEATH ANIMATION \\

            Destroy(gameObject);
            return;
        }
    }

    public void TakeDamage(float damageTaken)
    {
        if (health >= 0)
        {
            health -= damageTaken;
        }
    }
}
