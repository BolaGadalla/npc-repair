﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Configs
    [SerializeField] float runSpeed = 5f;
    [SerializeField] bool isAlive = true;
    [SerializeField] float health = 200;

    public Rigidbody2D player;
    public Animator animator;

    Vector2 movement;

    public int repairStrength = 20;

    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!isAlive) { return; }
        Run();
        Die();
    }

    /// <summary>
    /// Run method by using a horizontal velocity and playing the running animation at the same time
    /// </summary>
    private void Run()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Speed", movement.sqrMagnitude);
        player.MovePosition(player.position + movement * runSpeed * Time.fixedDeltaTime);
    }

    private void Die()
    {
        if (health <= 0)
        {
            isAlive = false;
            // PLAY DEATH ANIMATION \\
            // DISPLAY DEATH PANEL \\
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Turret turret = other.GetComponent<Turret>();
        if (Input.GetKeyUp(KeyCode.Space))
        {
            // PLAY REPAIR ANIMATION \\
            turret.IncreaseDurability(repairStrength);
        }
    }
}
