﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    // ENEMIES \\
    public GameObject[] enemies;
    public GameObject[] enemiesSpawnPoint;

    // TURRET \\
    public GameObject turret;
    public GameObject[] turretSpawnPoint;

    public int numberOfEnemiesToSpawn;

    private int randEnemiesSpawnPoint;
    private int randEnemiesPrefabs;
    private int randTurretSpawnPoint;

    void OnTriggerEnter2D(Collider2D other)
    {
        Player player = other.GetComponent<Player>();
        if (player != null)
        {
            for (int i = 0; i < numberOfEnemiesToSpawn; i++)
            {
                randEnemiesSpawnPoint = Random.Range(0, enemiesSpawnPoint.Length);
                randEnemiesPrefabs = Random.Range(0, enemies.Length);
                randTurretSpawnPoint = Random.Range(0, turretSpawnPoint.Length);
                // Player has opened the door
                // Instantiate an enemy at a random spawn point
                Instantiate(enemies[randEnemiesPrefabs], enemiesSpawnPoint[randEnemiesSpawnPoint].transform.position, Quaternion.identity);
                Destroy(enemiesSpawnPoint[randEnemiesSpawnPoint]);
                // Instantiate a turret at a random spawn point position
                Instantiate(turret, turretSpawnPoint[randTurretSpawnPoint].transform.position, Quaternion.identity);
                Destroy(turretSpawnPoint[randTurretSpawnPoint]);
            }
            Destroy(gameObject);
        }
    }
}
