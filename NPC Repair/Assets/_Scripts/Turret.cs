﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public GameObject projectile;
    public Transform firingPoint;

    public float durability = 200;
    private float fixedDurability;
    private float durabilityDecrease = 0.2f;
    public float turretHealth = 200;
    public bool isAlive = true;

    public float turretFiringSpeed;

    

    // Start is called before the first frame update
    void Start()
    {
        fixedDurability = durability;
        // PLAY INTRO ANIMATION \\
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) { return; }
        if (durability >= 0)
        {
            StartCoroutine(Firing());
        }
        durability -= durabilityDecrease * Time.deltaTime;
    }

    public void IncreaseDurability(int amount)
    {
        if (durability < fixedDurability)
        {
            durability += amount;
        }
    }

    // Decreases the durability of the turret
    public void DecreaseDurability(int amount)
    {
        if (durability >= 0)
        {
            durability -= amount;
        }
    }

    public void TakeDamage(int amount)
    {
        if (turretHealth >= 0)
        {
            turretHealth -= amount;
        }
    }

    /// <summary>
    /// This will fire a projectile
    /// </summary>
    public void Fire()
    {
        var bullit = Instantiate(projectile, firingPoint.transform.position, Quaternion.identity);
        // Get the rotation of the turret and that is the force
        bullit.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
        Destroy(bullit, 10f);
    }

    IEnumerator Firing()
    {
        // The seconds between each shot decreases as the duration decreases
        turretFiringSpeed = durability / turretFiringSpeed;
        yield return new WaitForSeconds(turretFiringSpeed * Time.deltaTime);
        Fire();
    }
}
